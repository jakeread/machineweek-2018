# Machine Building at the CBA

Hello! If you've landed here, it's likely that you're getting ready to design and build some equipment. This guide is part of the [MIT Center for Bits and Atoms' ongoing machines-making-machines effort](http://mtm.cba.mit.edu/), wherein we seek to turn the universe into a deeply recursive heirarchy of robots building one another.

Most machines are monolithic and static: they live their lives for one process, and are hard to modify for anything else. 

Machines described here are *parametric configurations of object-oriented hardware* that can be assembled into *instances* of equipment, whose constitutent parts are free for future addition and modification. 

Machine controllers are *networked collections of input and output devices* that contain bare minimum state - high level planning and interface takes place within *virtual machine controllers* that are similarly easy to assemble, configure, and tune.

![overview](video/3min-for-10min-enc.mp4)

### How To Play

Machine Design / Robotics can be *basically* broken up into three fields:
- **Hardware** (as in mechanism)
- **Electronics** (also 'hardware' to some people) 
- **Control**

Local to the CBA we have projects for each of these aspects, each having long history in the [MTM Project](http://mtm.cba.mit.edu/). 

#### [Hardware - RCT Gantries](https://gitlab.cba.mit.edu/jakeread/rctgantries) 
As in 'roller coaster tycoon' - we use roller bearings as guide elements. 

These are parametric linear axis that you can use to fabricate your own linear machines. The repo linked above has hardware documentation, parametric CAD models, and more explanation.

#### [Electronics - ATK - AutomataKit](https://gitlab.cba.mit.edu/jakeread/automatakit)

This is a collection of 'endpoints' for the network that makes up our machine controllers. I.E. each motor, sensor, what-have-you on the machine is given one tiny controller. That controller is responsible for doing very simple things: turning motors about, running low-level feedback control, reporting sensor data, etc. Machine Controller themselves (see next link) are *assemblies* of these tiny controllers, which we coordinate over a network. 

That network protocol / etc is described in the link above, along with links to the currently-available hardware endpoints (and their repositories). 

For machine week, you'll have four ATKStepper23's, two ATKBreadBoardBoards and one ATKRouter to hook it all up.

 - [ATKRouter](https://gitlab.cba.mit.edu/jakeread/atkrouter)
 - [ATKStepper17](https://gitlab.cba.mit.edu/jakeread/atkstepper17)
 - [ATKStepper23](https://gitlab.cba.mit.edu/jakeread/atkstepper23)
 - [ATKBreadBoardBoard](https://gitlab.cba.mit.edu/jakeread/atkbreadboardboard)
 - [ATKBLDCDriver](https://gitlab.cba.mit.edu/jakeread/atkbldcdriver)

#### [Control - RNDMC - Reconfigurable Numeric Dataflow Controller](https://gitlab.cba.mit.edu/jakeread/atkapi)

'RUN-DMC' - get it?

This is a piece of software that is designed to help you assemble higher level controllers from the modular pieces of networked hardware mentioned above. It serves a graphical programming interface, or can be used to write a regular old program (in javascript, as a kind of library). 

#### [Also This - Open Assemblies](http://openassemblies.com)

This is just a place where I put links to everything else, including this. If you're ever trying to find something, go here first. 

# Going About Designing a Machine

Any kind of design process is nonlinear / contradictory. To that end, this guide takes the form of an unordered list.

## 1) Design

I leave 'design' up to you, to save myself following the rabbit hole where I end up writing about it for too long. Look at [examples](http://mtm.cba.mit.edu), do back of envelope maths (stiffnesses, forces, speeds, weights) etc, draw things with your hands, with your friends, have ideas, etc. 

Another important note: the process posted here is proscriptive, but it isn't meant to be restrictive. I.E. we only have parametric designs for linear axis here, but there are *lots* of ways to make rotation happen, and the motors provided are torque-y. There are no rules, this is just an attempt at helping you do the thing!

[>>>>> Filippos has also documented his run through this process! <<<<<](http://fab.cba.mit.edu/classes/863.18/CBA/people/filippos/link/week-11.html)

## 2) CAD Wrangling 

### Configure Parametric Axis in Fusion 360 

Really, this happens once you know how long / wide you'd like each axis to be.

![](video/SEQ-fusion-parametric-enc.mp4)

To start, head to [the RCT Gantries Repository](https://gitlab.cba.mit.edu/jakeread/rctgantries) and *read it* and then download from the CAD folder the parametric axis you'd like to configure. 

In Fusion[^2], you can open this file up and use (from the top menu)

``` Modify >> Change Paremeters ```

Each of these models should have some parameters *starred*, these are what you'll want to configure. Go ahead and set axis lengths, material thicknesses according to what you're doing. When you're satisfied, you can export the model as a .step file, using the file menu, to prep it for fabrication.

**WARN** making beautifully parametric CAD designs is hard work. Some things might break - especially if you make drastic changes to parameters. 

``` File >> Export ``` 

Make sure to change 'type' to .step, and check the 'save to my computer' box. 

### Set Relations Between Axis in Rhino

Rhino is a great swiss-army knife tool for CAD wrangling. The linear axis we have here can kind of bolt-to-anything, but it will be useful to figure out / plan what / where / how we're going to bolt them together.

.step files open up beautifully in Rhino[^3], where you can go about setting up relationships between parametric elements. I.E. here is where you 'assemble' the components you've configured. 

![](video/SEQ-rhino-relationships-enc.mp4)

If you're more comfortable in Fusion, you can save configured axis as new files, and include them in an assembly, mating them together there. 

I've also made a set of static blocks that can be configured to connect degrees of freedom to one another, most usefully at 90 degrees. Those models are also available in [the RCT Gantries Repository](https://gitlab.cba.mit.edu/jakeread/rctgantries) - although at the time of writing there is only one available for 0.25" thick aluminium, it wouldn't take much to write a new model for 3/8" thick HDPE, or come up with your own mounting blocks. 

![](video/SEQ-rct-add-blk-enc.mp4)

I'll also leave the chassis up to you. You can design it in Fusion, or Rhino, whatever you'd like.

Of course, it's also fair game to do everything in Fusion and build a big parametric model (i.e. instances of parametric gantries could be imported to an assembly as components), if you'd like. Rhino is personal preference. Have I made this point already?

### Modifications / Connections in Rhino

![](video/SEQ-rhino-modifications-enc.mp4)

Rhino is pretty free-form, and presents a good opportunity to add-in whatever details you'd like - i.e. here I'm modifying the X-Gantry of [this machine](https://gitlab.cba.mit.edu/jakeread/mothermother) to lighten it up, and to mate with the Y-connectors on the same machine. I also add a cable-routing tray. 

The thing about parametric hardware is that it's kind of going to be like the vise-grips of robotics. Great for most stuff, not perfect for anything. Generality = mediocrity (if all we are interested in is *performance*). All this to say, trying to stay in the rigid bounds of a parametric system all the way through is often more effort than it's worth: at some point it becomes productive to abandon parametricism and just draw things. 

Of course, you can get away without doing very much of this at all - just make sure you have the right holes / mounts set up to secure each axis to eachother. 

## 3) Fabrication

### Lay Out Cut Files in Rhino

Once you're feeling O-K about your machine design, you should get ready to cut it out. 

Again, I do this in Rhino because I like to be able to push things around and nest curves by hand. 

You can also export faces directly from Fusion by:
1. right clicking on a face
2. creating a sketch on that face
3. rick-clicking on that sketch in the feature menu (left) 
4. selecting 'save as dxf'

![](video/SEQ-rhino-layout-fab-enc.mp4)

First, pick out the 3D Printed Parts and slice them up. Each axis has these 3D Printed Bits: 
 - Belt Holders (both sides)
 - Belt 'Tenders' (for the motor-adjacent rollers) - 4x

Then you're off to lay out all of the flat bits. This is a lot of manual model-moving-about and 'nesting'. I recommend drawing out some rectangles of the size you'll be cutting from to make sure you can fit everything into the stock you have available. Your favourite commands will be ``` Orient3Pt ``` , ``` Rotate3D ``` , and ``` Move ```.  

Then, the command that you'll want to use is ``` DupFaceBorder ``` - this will take the faces of your parts (with the RCT Gantries, etc, everything should render well into 2D Cuts only [i.e. no pockets anywhere]), and render them as linework. Then you can export this linework (probably as a .dxf) to whatever machine tool you'd like.

![](video/SEQ-cutting-out-enc.mp4)

While I cheat by using the CBA's Waterjet and Zund, there are a lot of ways you could go about cutting out the pieces of your machine. HDPE cuts beautifully on a shopbot using a 1/8" single-flute o-cutter, for instance. 

### Assembly 

For assembly, it's best to follow along the documentation on the [RCT Gantries](https://gitlab.cba.mit.edu/jakeread/rctgantries) page. 

![rctimg](https://gitlab.cba.mit.edu/jakeread/rctgantries/raw/master/images/RCTN23-hdpe-dwg.png)

Filippos has put some documentation together from his experience doing all of this... [>>>>> that documentation is here <<<<<](http://fab.cba.mit.edu/classes/863.18/CBA/people/filippos/link/week-11.html)

Hardware
 - most of the plastic parts go together using the 'No.6 Plastic Thread Forming Screws' - to use these, make sure you have a clearance hole (~ 4.5mm OD) and that you pre-drill the other side with a bit in the region of 2.5mm od.
 - the Nema 23 Motors are mounted with M5 SHCS, one mount hole can be tapped, the others are slots
 - rollers use Shoulder Bolts as axis, these have M6 taps. careful to add the right # of shims in the right places (they're important!) and don't over-tighten, as the M6 threads can pull out of the HDPE quickly 

Belts take some time to get right:
 - tensioning the belt can be a pain - pinch it down on the static end (smaller 'block') first, then get it around the motor, and attached (but not tight) on the other end (l-shaped block)
 - the motor is mounted on slotted arcs, this is setup so that one mount screw (M5s) can be pinned, and the others rotated, to tension the belt at the motor
 - the l-shaped block can also add tension: two of the screws whose axis are parallel to the gantry's motion are there to secure the belt in the block. the other two are there to push against the edge of the gantry, thereby pushing the belt block away from the other side, and adding tension
 - one way to get around this is to use *only one belt* instead of the two meshed belts. This will be 1/2 as stiff, but will cross the threshold of 'working' vs. 'not working'. 

## 4) Electronics

There are some notes on wiring up the controllers on the [automatakit](https://gitlab.cba.mit.edu/jakeread/automatakit) page.

![](video/SEQ-wiring-enc.mp4)

#### Circuits 

First, I would test each board, and make sure it will load code. Wire up some power to the board (using eye terminals and the 18ga wire provided - I solder these together) and make sure that one green LED lights up - this is connected directly between the +3v3 and GND line.

- the system is meant to run on 24v, any more voltage than this and the buck regulator might give up 

Once you can confirm each board gets power, you'll want to make sure they can program. Assign someone in the group to be the C-Code / Firmware Wizard, and get them to read up on how to program the boards [here](https://gitlab.cba.mit.edu/jakeread/automatakit/blob/master/reproduction/firmware.md).

Once you can program things, you'll be interested in soldering RJ45 Jacks onto them. OK. 

#### Boards, meet Motors

Next, attach motor boards to motors. You can use M3 Socket Head Screws with Spacers for this, or, if they show up in time, fancy standoffs.

Then you'll want to attach the boards to the motor leads: I strip the motor leads pretty short, and tin them. I also tin the motor pads on the stepper board. I use a multimeter to discover which pairs are coils [see stepper motor doc from adafruit](https://learn.adafruit.com/all-about-stepper-motors/types-of-steppers) - these are BiPolar. One coil should connect to 'A1' and 'A2', and the other, the others. Coil orientation is not imporant, it will only affect direction. 

#### Wire Routing 

Routing cables to everything else will take some finesse. **Making cable run smoothly as the machine moves about is something worth thinking about from the beginning** ! 

My attitude is to lay cables out starting from their terminal position, then winding them back to the first static point. Make [drag chain](https://en.wikipedia.org/wiki/Cable_carrier) type loops, testing for full range of motion without pulling on cables. Make sure that you're never exerting real mechanical load on connectors. Zip ties = friends.

## 5) Controllers

To control this thing, we use [RNDMC - RUN DMC - a Reconfigurable Numeric Dataflow Machine Controller](https://gitlab.cba.mit.edu/jakeread/rndmc). Documentation and usage is located in that << repo. 

# End Effectors

I'm working on a few end effectors. You can grab some of these design files and fabricate them, or try designing your own. Here's the [simple spindle](https://gitlab.cba.mit.edu/jakeread/simplespindle):

![spindle](https://gitlab.cba.mit.edu/jakeread/simplespindle/raw/master/images/ee-spindle-bldc.jpg)

Hopefully to come:
 - rotary tool w/ inserts a-la Zund
 - Piezo Touch Probe 

# BOM 

This is a general BOM. For How to Make Almost Anything coordinators, CBA will coordinate ordering material. Section Heads should order HDPE sheets (below, in Material section) to be delivered to their labs, and if shopbots are not 4x8' size, change for 4x4' sheets. You should also make sure you have the right tools in your shops; I've included a list of useful or rare items here as well. 

## For Section Heads

If you think you're interested in making some aluminum machines, find 0.25" stock someplace: we use [Admiral Metals](https://www.admiralmetals.com/products-services/aluminum/) most of the time, McMaster works in a pinch but prices are high. I tend to get 12" x 48" stock sheets. This is a great option if you have a waterjet (or have a friend with a waterjet, i.e. Neil) and don't mind the extra overhead of working with stiff materials: tapping things, cost, etc. But as Ben Jennet reminded me, HDPE has 'ah' GPa of Tensile Modulus (one) and 6061 is ~ 69 GPa, so, lots more stiffness. It would be cool to see labs walking away with useful machines this year. If you get aluminum, also get some HDPE - it's still useful for chassis bits. 

Otherwise, McMaster will deliver 4x8' sheets of 3/8" stock HDPE (link below), one is probably enough as long as you don't want to make anything spectacularely large.

Also, make sure you have the right tools. Most of these things should already reside in your shop, I'm keeping a list here just in case. Mostly, there is a 1/8" 'O-Cutter' that machines HDPE like a dream, I recommend having two or three of those on hand...  Also some taps.

Everything else I can give to you at the next staff meeting: probably in a big-ish box, but not monstrous. I hope that's OK.

### Material

What | QTY | Link
--- | --- | ---
Aluminum 1/4" | ~ 4 - 5 12 x 48" Sheets | [Admiral](https://www.admiralmetals.com/products-services/aluminum/)
HDPE 3/8" / 0.375" | 1-2 Sheets | [McMaster](https://www.mcmaster.com/8619k438)
PLA for 3D Printing | < 1kg | [My Favorite](https://www.matterhackers.com/store/l/pro-series-natural-pla-filament-1.75mm/sk/MACKAE6L)

### Tools

What | Where Used | Link or McMaster
--- | --- | ---
Onsrud 1/8" Upcut Spiral Super-O | Milling HDPE Like a Dream | [Blackhawk Industrial](https://www.bhid.com/itemdetail/ONSCUT%2065-013)
M3 Tap | Extensively for Aluminum Parts | 2673A71
M5 Tap | Extensively for Aluminum Parts | 2673A74
M6 Tap | Shoulder Bolts | 2673A75
Countersink Bit | Flush Mounting | 27535A48
Hex Driver Set Metric | Cannonical | 5709A18
Torx Driver Set | Nice, Not Necessary | 6370A1
Torx Drill Driver T15 | Wonderful to have for Chassis; No. 6 Button Head Screws | 7396A42 
Torx Drill Driver T10 | For Flat-Head No. 6 Screws | 7396A41 

## The Rest of It

## Hardware for One NEMA23 Axis with 0.375" HDPE

Type | Size | QTY | Where Used | McMaster PN
--- | --- | --- | --- | --- 
Button Head Thread-Forming | No. 6, 3/4" | 10 + (4 * rail tab) (lots) | Connecting Lap and Tab HDPE, Belt Blocks, Chassis | 99512A265
Button Head Thread-Forming | No. 6, 1/2" | 2 | Belt Blocks | 99512A259
Flat Head Thread-Forming | No. 6, 3/4" | 8 | Flush Mounting HDPE | 95893A258
SHCS | M3x20 | 1 | Belt Tensioning at Motor | 91292A123
SHCS | M3x30 | 2 | Used *only* when pre-loading bearing rollers | 91292A022
SHCS | M3x40 | 2 | Used *only* when pre-loading bearing rollers | 91292A024
Belleville Washer | 3.1mm ID | 24 | Used *only* when pre-loading bearing rollers | 96445K157
Locknut | M3, Nylon | 6 | Used *only* when pre-loading bearing rollers | 90576A102
SHCS | M5x10 | 1 | Connecting Nema 23 Motor | 91292A124
SHCS | M5x16 | 3 | Nema 23 Motor through tensioning arcs | 91292A126
Shoulder Screw | 8mm Shoulder x 8mm x M6 | 10 | Guide Roller Shaft | 92981A198
Shoulder Screw | 8mm Shoulder x 16mm x M6 | 2 | Belt Guide Roller Shaft | 92981A202
Bearing Shim | 8mm ID x 10mm OD x 1mm Thick | 38 | Roller Separation | 98089A381 

## Purchase Parts 

What | Spec | QTY | Where Used | Link
--- | --- | --- | --- | --- 
608ZZ Bearings | 8x22x7 | 100 | Rollers | [VXB 10](https://www.vxb.com/608ZZ-Shielded-8x22x7-Miniature-Bearing-Pack-of-10-p/608zz10.htm), [VXB 1000](https://www.vxb.com/Wholesale-Lot-of-1000-608ZZ-Ball-Bearing-p/608zz-wholesale.htm)
GT2 Belt | 10mm Wide, Length Dependent | 2 | Belt! | [Amazon](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)
GT2 Pulley | 10mm Wide, Motor Bore Diameter | 1 | Transmission! | [Above, Combo](https://www.amazon.com/Timing-Pulley-Teeth-6-35mm-Printer/dp/B07BS4DVR5/)
Power Supply | 24v 350W Mean Well | or below | Power ! | [Amazon](https://www.amazon.com/Enclosed-LRS-350-24-Meanwell-Switching-LRS-350Series/dp/B07BK27V4W/)
Power Supply | 24v 500W Letour | 1 | Power | [Amazon](https://www.amazon.com/LETOUR-96V-240V-Converter-S-500W-24-Lighting/dp/B01HTF1Q06/)
Stepper Motor | NEMA23 x52mm | 2 | Torque ! | [StepperOnline](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-18deg-09-nm-1275ozin-2a-36v-57x57x52mm-4-wires-23hs20-2004s.html)
Stepper Motor | NEMA23 x76mm | 2 | Torque ! | [StepperOnline](https://www.omc-stepperonline.com/hybrid-stepper-motor/nema-23-bipolar-18deg-19nm-269ozin-28a-32v-57x57x76mm-4-wires-23hs30-2804s.html)

## Wiring

Assuming you're controlling this thing with [automatakit](https://gitlab.cba.mit.edu/jakeread/automatakit), these are the parts you'll want when you're wiring it up.

What | Spec | QTY | Digikey PN
--- | --- | --- | --- 
Power Entry | IEC 320-C14 10A | 1 | 486-3979-ND  
Power Entry Fuse | 10A | 1 | 486-1226-ND
RJ45 Plugs | RJ45 Modular 8p8c, For Flat Cable, IDC | 20 | AE10316-ND 
RJ45 Cable | 8 Conductor 26AWG Ribbon with Jacket | 100ft | A0082R-100-ND 
RJ45 Tool | Crimping | 1 | K582-ND
DC Power Terminal | M3 Stud Terminal 18-20AWG | 20 | 277-11144-ND 
18AWG Hook-Up GND | 18AWG Stranded with Silicone Jacket, Black | 100ft | CN101B-100-ND  
18AWG Hook-Up V++ | 18AWG Stranded with Silicone Jacket, Red | 100ft | CN101R-100-ND
Zip Ties | ~ 6" | 250 | Q731-ND

# FNs

[^1]: Typically, CNC Machines - but ambitiously I would like to include most of robotics and automation here as well. 
[^2]: Parametric CAD Software [from Autodesk](https://www.autodesk.com/products/fusion-360/overview), available [free for students and educators](https://www.autodesk.com/products/fusion-360/students-teachers-educators). 
[^3]: Non-Parametric CAD software [available from McNeel](https://www.rhino3d.com/), loved by generalists and computational geometry-ists. Educational licenses available. 